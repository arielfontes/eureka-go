package iputils

import (
	"gitlab.com/arielfontes/eureka-go/conf"
	"github.com/phayes/freeport"
	"log"
	"net"
)

func GetLocalIP() string {
	addrs, err := net.InterfaceAddrs()
	if err != nil {
		return ""
	}
	for _, address := range addrs {
		if ipnet, ok := address.(*net.IPNet); ok && !ipnet.IP.IsLoopback() {
			if ipnet.IP.To4() != nil {
				return ipnet.IP.String()
			}
		}
	}
	return ""
}

func GetFreePort(conf conf.Config) int {
	var err error
	port := conf.Server.Port

	if port != 0 {
		return port
	}

	port, err = freeport.GetFreePort()
	if err != nil {
		log.Panicf("Erro ao atribuir porta ", err)
	}

	return port
}