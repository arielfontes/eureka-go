package main

import (
	"fmt"
	"gitlab.com/arielfontes/eureka-go/conf"
	"gitlab.com/arielfontes/eureka-go/eureka"
	"gitlab.com/arielfontes/eureka-go/iputils"
	"log"
	"net/http"
	"net/http/httputil"
	"net/url"
	"sync"
)

var config conf.Config

func main() {
	config = conf.GetConfig()
	port := iputils.GetFreePort(config)

	eureka.StartEurekaRegistration(config.Eureka.AppName, port, config.Eureka.EurekaUrl)

	go startProxy(port)

	wg := sync.WaitGroup{}
	wg.Add(1)
	wg.Wait()
}

func startProxy(port int) {
	http.HandleFunc("/", redirect)

	log.Println("application started on port", port)
	err := http.ListenAndServe(fmt.Sprintf(":%d", port), nil)
	if err != nil {
		log.Panicf("erro ao startar sidecar %v", err)
	}
}

func redirect(w http.ResponseWriter, r *http.Request) {
	target, _ := url.Parse(config.Eureka.TargetAppUrl)
	httputil.NewSingleHostReverseProxy(target).ServeHTTP(w, r)
}
