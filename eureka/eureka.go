package eureka

import (
	"fmt"
	"gitlab.com/arielfontes/eureka-go/iputils"
	"github.com/hudl/fargo"
	"github.com/twinj/uuid"
	"log"
	"os"
	"os/signal"
	"syscall"
	"time"
)

var uid string

func StartEurekaRegistration(appName string, port int, eurekaUrl string) {
	localIp := iputils.GetLocalIP()
	uid = uuid.NewV4().String()

	app := &fargo.Instance {
		HostName:         localIp,
		Port:             port,
		PortEnabled:	  true,
		App:              appName,
		IPAddr:           localIp,
		VipAddress:       appName,
		SecureVipAddress: appName,
		HealthCheckUrl:   fmt.Sprintf("http://%s:%d", localIp, port) + "/health",
		StatusPageUrl:    fmt.Sprintf("http://%s:%d", localIp, port) + "/info",
		HomePageUrl:      fmt.Sprintf("http://%s:%d", localIp, port) + "/",
		Status:           fargo.UP,
		DataCenterInfo:   fargo.DataCenterInfo{Name: fargo.MyOwn},
		LeaseInfo:        fargo.LeaseInfo{RenewalIntervalInSecs: 30},
		UniqueID:         generateId,
	}

	app.SetMetadataString("instanceId", app.Id())

	e := fargo.NewConn(eurekaUrl)
	e.UseJson = true

	err := e.RegisterInstance(app)
	if err != nil {
		log.Panicf("Error on registering on Eureka %v", err)
	}

	handleSigterm(app, e)

	go startHeartbeat(app, e)
}

func startHeartbeat(app *fargo.Instance, conn fargo.EurekaConnection) {
	for {
		time.Sleep(time.Second * 30)
		err := conn.HeartBeatInstance(app)
		if err != nil {
			conn.ReregisterInstance(app)
		}
	}
}

func handleSigterm(app *fargo.Instance, conn fargo.EurekaConnection) {
	c := make(chan os.Signal, 1)

	signal.Notify(c, os.Interrupt)
	signal.Notify(c, syscall.SIGTERM)

	go func() {
		<-c
		err := conn.DeregisterInstance(app)
		if err != nil {
			log.Panicf("Error on deregistering on Eureka %v", err)
		}
		os.Exit(1)
	}()
}

func generateId(i fargo.Instance) string {
	return fmt.Sprintf("%s:%s:%s", i.HostName, i.App, uid)
}