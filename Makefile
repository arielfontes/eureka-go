GOCMD=go
BUILD=build
DOCKERCMD=docker

build:
	CGO_ENABLED=0 GOOS=linux $(GOCMD) $(BUILD) -a -installsuffix cgo -o springo

docker-build: build
	mv springo docker/
	$(DOCKERCMD) $(BUILD) -t arielfontes/springo:0.1 docker
	rm docker/springo