package conf

import (
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"log"
)

type Config struct {
	Eureka struct{
		EurekaUrl string `yaml:"url"`
		TargetAppUrl string `yaml:"targetAppUrl"`
		AppName string `yaml:"appName"`
	}

	Server struct{
		Port int `ỳaml:"port"`
	}
}

func GetConfig() Config {
	c := Config{}

	yamlFile, err := ioutil.ReadFile("conf.yml")
	if err != nil {
		log.Panicf("conf.yml not found. %v ", err)
	}
	err = yaml.Unmarshal(yamlFile, &c)
	if err != nil {
		log.Panicf("invalid configuration. cant unmarshal: %v", err)
	}

	return c
}
